//
//  TableViewCell.m
//  bTOdo
//
//  Created by Владимир Кубанцев on 09.11.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "TableViewCell.h"
#import "StrikethroughLable.h"

@implementation TableViewCell
{
    CAGradientLayer* _gradientLayer;
    CGPoint _originalCenter;
    StrikethroughLable *_label;
    CALayer *_itemCompleteLayer;
    BOOL _markCompleteOnDragRelease;
    BOOL _deleteOnDragRelease;
    const float LABEL_LEFT_MARGIN;
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
         const float LABEL_LEFT_MARGIN = 15.0f;
        _label = [[StrikethroughLable alloc] initWithFrame:CGRectNull];
        _label.textColor = [UIColor whiteColor];
        _label.font = [UIFont boldSystemFontOfSize:16];
        _label.backgroundColor = [UIColor clearColor];
        [self addSubview:_label];
        //self.selectionStyle = UITableViewCellSelectionStyleNone;
        // add a layer that overlays the cell adding a subtle gradient effect
//        _gradientLayer = [CAGradientLayer layer];
//        _gradientLayer.frame = self.bounds;
//        _gradientLayer.colors = @[(id)[[UIColor colorWithWhite:1.0f alpha:0.2f] CGColor],
//                                  (id)[[UIColor colorWithWhite:1.0f alpha:0.1f] CGColor],
//                                  (id)[[UIColor clearColor] CGColor],
//                                  (id)[[UIColor colorWithWhite:0.0f alpha:0.1f] CGColor]];
//        _gradientLayer.locations = @[@0.00f, @0.01f, @0.95f, @1.00f];
        
//        [self.layer insertSublayer:_gradientLayer atIndex:0];
        
        _itemCompleteLayer = [CALayer layer];
        _itemCompleteLayer.backgroundColor = [[[UIColor alloc] initWithRed:0.0 green:0.6 blue:0.0 alpha:1.0] CGColor];
        _itemCompleteLayer.hidden = YES;
        [self.layer insertSublayer:_itemCompleteLayer atIndex:0];
        
        UIGestureRecognizer* recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        recognizer.delegate = self;
        [self addGestureRecognizer:recognizer];
    }
    return self;
}

- (void)setTodoItem:(ToDoItem *)todoItem {
    _todoItem = todoItem;
    _label.text = todoItem.text;
    _label.strikethrough = todoItem.complete;
    _itemCompleteLayer.hidden = !todoItem.complete;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    // ensure the gradient layers occupies the full bounds
    _gradientLayer.frame = self.bounds;
    _itemCompleteLayer.frame = self.bounds;
    _label.frame = CGRectMake(15, 0, self.bounds.size.width - 15, self.bounds.size.height);
}

#pragma mark - horizontal pan gesture methods
- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer {
    CGPoint translation = [gestureRecognizer translationInView:[self superview]];
    if (fabs(translation.x) > fabs(translation.y)) {
        return YES;
    }
    return NO;
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        _originalCenter = self.center;
    }
    if (recognizer.state == UIGestureRecognizerStateChanged) {

        CGPoint translation = [recognizer translationInView:self];
        self.center = CGPointMake(_originalCenter.x + translation.x, _originalCenter.y);
        _deleteOnDragRelease = self.frame.origin.x < -self.frame.size.width / 2;
        _markCompleteOnDragRelease = self.frame.origin.x > self.frame.size.width / 2;
        //_markCompleteOnDragRelease = YES;
    }
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {

        CGRect originalFrame = CGRectMake(0, self.frame.origin.y, self.bounds.size.width, self.bounds.size.height);
        
        if (!_deleteOnDragRelease) {
            
            [UIView animateWithDuration:0.2
                             animations:^{self.frame = originalFrame;
                             }
             ];
            //[self.delegate ToDoItemDeleted:self.todoItem];
        }
        if (_markCompleteOnDragRelease) {
            self.todoItem.complete = YES;
            _itemCompleteLayer.hidden = NO;
            _label.strikethrough = YES;
        }
    }
    
}

@end
