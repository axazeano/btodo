//
//  TableViewCell.h
//  bTOdo
//
//  Created by Владимир Кубанцев on 09.11.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewCellDelegate.h"

@interface TableViewCell : UITableViewCell

@property (nonatomic) ToDoItem *todoItem;

@property (nonatomic, assign) id<TableViewCellDelegate> delegate;

@end
