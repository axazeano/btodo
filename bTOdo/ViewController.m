//
//  ViewController.m
//  bTOdo
//
//  Created by Владимир Кубанцев on 09.11.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#import "ToDoItem.h"
#import "ViewController.h"
#import "TableViewCell.h"
#import "TableViewCellDelegate.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate, TableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property (nonatomic, strong) NSMutableArray* toDoItems;

@end

@implementation ViewController

//-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//           }
//    return self;
//}

- (void)viewDidLoad {
    [super viewDidLoad];

    // TODO: _toDoItems -> self.toDoItems
    _toDoItems = [[NSMutableArray alloc] init];
    [_toDoItems addObject:[ToDoItem toDoItemWithText:@"Feed the cat"]];
    [_toDoItems addObject:[ToDoItem toDoItemWithText:@"Buy eggs"]];
    [_toDoItems addObject:[ToDoItem toDoItemWithText:@"Pack bags for WWDC"]];
    [_toDoItems addObject:[ToDoItem toDoItemWithText:@"Rule the web"]];
    [_toDoItems addObject:[ToDoItem toDoItemWithText:@"Buy a new iPhone"]];
    [_toDoItems addObject:[ToDoItem toDoItemWithText:@"Find missing socks"]];
    [_toDoItems addObject:[ToDoItem toDoItemWithText:@"Write a new tutorial"]];
    [_toDoItems addObject:[ToDoItem toDoItemWithText:@"Master Objective-C"]];
    [_toDoItems addObject:[ToDoItem toDoItemWithText:@"Remember your wedding anniversary!"]];
    [_toDoItems addObject:[ToDoItem toDoItemWithText:@"Drink less beer"]];
    [_toDoItems addObject:[ToDoItem toDoItemWithText:@"Learn to draw"]];
    [_toDoItems addObject:[ToDoItem toDoItemWithText:@"Take the car to the garage"]];
    [_toDoItems addObject:[ToDoItem toDoItemWithText:@"Sell things on eBay"]];
    [_toDoItems addObject:[ToDoItem toDoItemWithText:@"Learn to juggle"]];
    [_toDoItems addObject:[ToDoItem toDoItemWithText:@"Give up"]];

    // TODO: TableView -> tableView
    self.TableView.dataSource = self;
    [self.TableView registerClass:[TableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.TableView reloadData];
    self.TableView.delegate = self;
    self.TableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.TableView.backgroundColor = [UIColor blackColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource protocol methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _toDoItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *ident = @"cell";
    // re-use or create a cell
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ident forIndexPath:indexPath];
//    cell.textLabel.backgroundColor = [UIColor clearColor];
    NSInteger index = [indexPath row];
    ToDoItem *item = self.toDoItems[index]; //[self.toDoItems objectAtIndex:index];
//    ToDoItem *item = [[ToDoItem alloc] initWithText:[_toDoItems objectAtIndex:index]];
//    ToDoItem *item = [_toDoItem objectAtIndex:index];
    //cell.textLabel.text = item.text;
    cell.delegate = self;
    cell.todoItem = item;
    [cell setNeedsDisplay];
    return cell;
}

- (UIColor*)colorForIndex:(NSInteger) index {
    NSUInteger itemCount = self.toDoItems.count - 1;
    float val = ((float)index / (float)itemCount) * 0.6;
    return [UIColor colorWithRed:1.0 green:val blue:0.0 alpha:1.0];
}

#pragma mark - UITableViewDataDelegate protocol methods
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0f;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [self colorForIndex:[indexPath row]];
}

- (void)ToDoItemDeleted:(ToDoItem *)ToDoItem {
    NSInteger index = [_toDoItems indexOfObject:ToDoItem];
    [self.TableView beginUpdates];
    [_toDoItems removeObject:ToDoItem];
    [self.TableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    [self.TableView endUpdates];
}

@end
