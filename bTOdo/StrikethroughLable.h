//
//  StrikethroughLable.h
//  bTOdo
//
//  Created by Владимир Кубанцев on 13.11.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StrikethroughLable : UILabel
@property (nonatomic) BOOL strikethrough;
@end
