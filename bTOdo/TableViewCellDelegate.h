//
//  SHCTableViewCellDelegate.h
//  bTOdo
//
//  Created by Владимир Кубанцев on 11.11.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#ifndef SHCTableViewCellDelegate_h
#define SHCTableViewCellDelegate_h
#import "ToDoItem.h"

@protocol TableViewCellDelegate <NSObject>

- (void) ToDoItemDeleted:(ToDoItem *)ToDoItem;

@end


#endif /* SHCTableViewCellDelegate_h */
