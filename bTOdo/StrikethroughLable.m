//
//  StrikethroughLable.m
//  bTOdo
//
//  Created by Владимир Кубанцев on 13.11.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#import "StrikethroughLable.h"
#import <QuartzCore/QuartzCore.h>

@implementation StrikethroughLable {
    CALayer* _strikethroughLayer;
}

const float STRIKEOUT_THICKNESS = 2.0f;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _strikethroughLayer = [CALayer layer];
        _strikethroughLayer.backgroundColor = [[UIColor whiteColor] CGColor];
        _strikethroughLayer.hidden = YES;
        [self.layer addSublayer:_strikethroughLayer];
    }
    return self;
}

- (void)layoutSubview {
    [super layoutSubviews];
    [self resizeStrikeThrough];
}

- (void)resizeStrikeThrough {
    CGSize textSize = [self.text sizeWithAttributes:@{NSFontAttributeName:
                                                          [UIFont systemFontOfSize:17.0f]}];
    _strikethroughLayer.frame = CGRectMake(0, self.bounds.size.height/2, textSize.width, STRIKEOUT_THICKNESS);
}

#pragma mark - property setter
- (void)setStrikethrough:(BOOL)strikethrough {
    _strikethrough = strikethrough;
    _strikethroughLayer.hidden = !strikethrough;
}

@end
