//
//  ToDoItem.m
//  bTOdo
//
//  Created by Владимир Кубанцев on 09.11.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#import "ToDoItem.h"

@implementation ToDoItem

- (instancetype)initWithText:(NSString *)text {
    self = [super init];
    if (self) {
        self.text = text;
    }
    return self;
}

+ (instancetype)toDoItemWithText:(NSString *)text {
    return [[ToDoItem alloc] initWithText:text];
}
@end
