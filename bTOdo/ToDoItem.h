//
//  ToDoItem.h
//  bTOdo
//
//  Created by Владимир Кубанцев on 09.11.15.
//  Copyright © 2015 Владимир Кубанцев. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ToDoItem : NSObject
// Text description
@property (nonatomic, copy) NSString *text;
// Is completed
@property (nonatomic) BOOL complete;


+ (instancetype)toDoItemWithText:(NSString *)text;
- (instancetype)initWithText:(NSString *)text;
@end
